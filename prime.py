from pathlib import Path
import time
import os

# clear the terminal
os.system('cls')


startTime = time.process_time()

def firstToLast(lb):
        # https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
        places = sorted(lb.items(), key=lambda x: x[1], reverse=True)

        # all me baby
        sortedLB = {}
        for i in places:
            sortedLB[i[0]] = i[1]
        return sortedLB


def numsNoRepeat(nums):
    result = {}
    for i in nums:
        if not i in result:
            result[i] = 1
        else:
            result[i] = result[i] + 1

    return result

pathToDir = Path(__file__).parent.absolute().__str__()

pathToFile = Path(f'{pathToDir}\\file.txt')


def isPrime(number):
    prime = False
    if number == 2:
        prime = True
    elif number == 1:
        prime = False
    elif number == 3:
        prime = True
    elif number == 5:
        prime = True
    elif number == 7:
        prime = True
    else:
        # is the number divisible evenly by every number between 2 and half of itself
        for i in range(2, ((number//2) + 2)):
            numDivided = number % i
            if (numDivided != 0):
                prime = True
            else:
                prime = False
                break
    return prime


primes = []
currNum = 0

endNumber = 400000


for j in range(endNumber / 100):
    primes.append([])

    for i in range(100):
        currNum += 1
        if isPrime(currNum):
            primes[j].append(currNum)

allPrimes = []
lens = []
with open(pathToFile, 'w') as f:
    for num in range(len(primes)):
        for i in primes[num]:
            allPrimes.append(i)
        lens.append(len(primes[num]))
        f.write(f'{len(primes[num])}\n')

    f.close()



results = numsNoRepeat(lens)

for i in results:
    print(f'number of times that {i} primes happened per 100 {results[i]}')


timeToRun = time.process_time() - startTime
print(f'{timeToRun} seconds')
